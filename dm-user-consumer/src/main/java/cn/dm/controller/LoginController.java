package cn.dm.controller;


import cn.dm.dto.Dto;
import cn.dm.service.UserFeignClient;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class LoginController {
    @Resource
    private UserFeignClient userFeignClient;
    public Dto userLogin(@RequestParam("phone")String phone, @RequestParam("password")String password)throws Exception{
       return userFeignClient.userLogin(phone,password);
    }
}
