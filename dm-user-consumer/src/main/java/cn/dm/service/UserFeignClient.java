package cn.dm.service;

import cn.dm.dto.Dto;
import cn.dm.pojo.DmUser;
import cn.dm.service.impl.UserFeignClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "dm-user-provider", fallback = UserFeignClientFallback.class)
public interface UserFeignClient {

public Dto userLogin(String phone,String password)throws Exception;

public Dto userOut()throws Exception;

public Dto userAdd(DmUser dmUser)throws Exception;
}
