package cn.dm.service.impl;

import cn.dm.client.RestDmCinemaClient;
import cn.dm.client.RestDmImageClient;
import cn.dm.client.RestDmUserClient;
import cn.dm.common.*;
import cn.dm.dto.Dto;
import cn.dm.dto.DtoUtil;
import cn.dm.pojo.DmImage;
import cn.dm.pojo.DmUser;
import cn.dm.service.UserFeignClient;
import cn.dm.vo.DmUserVO;
import cn.dm.vo.TokenVO;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserFeignClientFallback implements UserFeignClient {
    @Resource
    private RestDmUserClient restDmUserClient;
    @Resource
    private RestDmImageClient restDmImageClient;
    @Resource
    private RedisUtils redisUtils;

    @Override
    public Dto userLogin(String phone, String password) throws Exception {
        Map<String,Object> param=new HashMap<>();
        param.put("phoen",phone);
        param.put("password",password);
        List<DmUser> dmUserList=restDmUserClient.getDmUserListByMap(param);
        if(EmptyUtils.isEmpty(dmUserList)){
            return DtoUtil.returnFail("账号密码错误","1006");
        }else{
            DmUser dmUser=dmUserList.get(0);
            String userJson=(String)redisUtils.get(Constants.USER_TOKEN_PREFIX+dmUser.getId());
            if(EmptyUtils.isEmpty(userJson)){
                Map<String,Object> imgParam=new HashMap<>();
                imgParam.put("targetId",dmUser.getId());
                imgParam.put("category",Constants.Image.ImageCategory.user);
                DmImage dmImage=restDmImageClient.getDmImageListByMap(imgParam).get(0);
                DmUserVO dmUserVO=new DmUserVO();
                BeanUtils.copyProperties(dmUser,dmUserVO);
                dmUserVO.setUserId(dmUser.getId());
                dmUserVO.setImageId(dmImage.getId());
                dmUserVO.setImgUrl(dmImage.getImgUrl());
                TokenVO tokenVO=crateTkoen(dmUser);
                Object[] dtoObject={dmUserVO,tokenVO};
                String userVOJSON= JSONObject.toJSONString(dmUserVO);
                redisUtils.set(dmUser.getId().toString(),Constants.Redis_Expire.SESSION_TIMEOUT,userVOJSON);
                redisUtils.set(Constants.USER_TOKEN_PREFIX+dmUser.getId(),
                        Constants.Redis_Expire.SESSION_TIMEOUT,tokenVO.getToken());
                return DtoUtil.returnDataSuccess(dtoObject);

            }else{
                return DtoUtil.returnFail("用户已登录","0001");

            }
        }
    }

    @Override
    public Dto userOut() throws Exception {
        return null;
    }

    @Override
    public Dto userAdd(DmUser dmUser) throws Exception {
        return null;
    }

    private TokenVO crateTkoen(DmUser dmUser){
        String token="DM-"+ MD5.getMd5(dmUser.getPhone()+dmUser.getId(),16);
        TokenVO tokenVO=new TokenVO();
        tokenVO.setToken(token);
        tokenVO.setExpTime(Constants.Redis_Expire.SESSION_TIMEOUT);
        tokenVO.setGenTime(System.currentTimeMillis());
        return tokenVO;
    }



}
