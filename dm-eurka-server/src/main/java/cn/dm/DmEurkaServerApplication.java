package cn.dm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DmEurkaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DmEurkaServerApplication.class, args);
    }

}
