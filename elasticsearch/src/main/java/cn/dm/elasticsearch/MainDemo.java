package cn.dm.elasticsearch;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.omg.CORBA.SetOverrideType;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MainDemo {
    private TransportClient transportClient=null;

    //连接方法
    public void getConnection() throws UnknownHostException{//112.132
        Settings settings=Settings.builder().put("cluster.name","elasticsearch-application").build();
        transportClient=new PreBuiltTransportClient(settings);
        //通过设置ip地址与端口号进行服务的连接
        transportClient.addTransportAddress(new TransportAddress(InetAddress.getByName("192.168.112.132"),9300));
    }

    //创建新的索引
    public void addindex() throws IOException{
        //创建索引
        transportClient.admin().indices().prepareCreate("school").get();
        //配置索引的type
        XContentBuilder xContentBuilder=XContentFactory.jsonBuilder()
                .startObject()
                .startObject("properties")
                .startObject("userName")
                .field("type","text")
                .endObject()
                .endObject()
                .endObject();
        transportClient.admin().indices().preparePutMapping("school")
                .setType("T43")
                .setSource(xContentBuilder).get();
        //关闭
        transportClient.close();
    }

    //数据查询
    public void getInfo(){
        SearchRequestBuilder searchRequestBuilder=transportClient.prepareSearch("school");
        //全文
        searchRequestBuilder.setQuery(QueryBuilders.matchQuery("userName","张"));
        //精确
        searchRequestBuilder.setQuery(QueryBuilders.termQuery("userName","张"));
        //获得结果
        SearchResponse searchResponse=searchRequestBuilder.execute().actionGet();
        SearchHits searchHits=searchResponse.getHits();
        SearchHit[] searchHitArray=searchHits.getHits();
        if(searchHitArray!=null&&searchHitArray.length!=0){
            for(SearchHit searchHit:searchHitArray){
                System.out.println(searchHit.getSourceAsString());
            }
        }
    }

    //添加数据
    public void addDoc() throws IOException{
        XContentBuilder xContentBuilder=XContentFactory.jsonBuilder()
                .startObject()
                .field("userName","张三")
                .endObject();
        transportClient.prepareIndex("school","T43","01")
                .setSource(xContentBuilder).get();
        transportClient.close();
    }

    public static void main(String[] args){
        MainDemo mainDemo=new MainDemo();
        try {
            mainDemo.getConnection();
            mainDemo.addindex();
            mainDemo.addDoc();
            mainDemo.getInfo();
        }catch (UnknownHostException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("运行结束");
    }
}
