package cn.dm.service;

import cn.dm.dto.Dto;
import cn.dm.vo.ItemDetailVo;

public interface ItemLocalService {

    public Dto getItemInfo(Long id) throws Exception;

    public Dto getScheduler(Long itemid) throws Exception;
}
