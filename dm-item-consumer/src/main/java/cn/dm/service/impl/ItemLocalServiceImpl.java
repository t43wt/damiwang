package cn.dm.service.impl;

import cn.dm.client.RestDmCinemaClient;
import cn.dm.client.RestDmImageClient;
import cn.dm.client.RestDmItemClient;
import cn.dm.client.RestDmSchedulerClient;
import cn.dm.common.Constants;
import cn.dm.dto.Dto;
import cn.dm.dto.DtoUtil;
import cn.dm.pojo.DmCinema;
import cn.dm.pojo.DmImage;
import cn.dm.pojo.DmItem;
import cn.dm.pojo.DmScheduler;
import cn.dm.service.ItemLocalService;
import cn.dm.vo.ItemDetailVo;
import cn.dm.vo.ItemSchedulerVo;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemLocalServiceImpl implements ItemLocalService {
    @Resource
    private RestDmItemClient restDmItemClient;
    @Resource
    private RestDmCinemaClient restDmCinemaClient;
    @Resource
    private RestDmImageClient restDmImageClient;
    @Resource
    private RestDmSchedulerClient restDmSchedulerClient;
    @Override
    public Dto getItemInfo(Long id) throws Exception {

        DmItem dmItem=restDmItemClient.getDmItemById(id);

        DmCinema dmCinema=restDmCinemaClient.getDmCinemaById(dmItem.getCinemaId());

        Map<String,Object> param=new HashMap<String, Object>();
        param.put("targetId",dmItem.getId());
        param.put("category", Constants.Image.ImageCategory.item);
        DmImage dmImage=restDmImageClient.getDmImageListByMap(param).get(0);

        ItemDetailVo itemDetailVo=new ItemDetailVo();
        BeanUtils.copyProperties(dmItem,itemDetailVo);
        itemDetailVo.setStartTime(dmItem.getStartTime().toString());
        itemDetailVo.setEndTime(dmItem.getEndTime().toString());
        BeanUtils.copyProperties(dmCinema,itemDetailVo);
        itemDetailVo.setImgUrl(dmImage.getImgUrl());
        return DtoUtil.returnDataSuccess(itemDetailVo);
    }

    @Override
    public Dto getScheduler(Long itemid) throws Exception {
        Map<String,Object> param=new HashMap<String,Object>();
        param.put("itemid",itemid);
        List<DmScheduler> dmSchedulerList=restDmSchedulerClient.getDmSchedulerListByMap(param);
        List<ItemSchedulerVo> itemSchedulerVoList=new ArrayList<ItemSchedulerVo>();
        for(DmScheduler dmScheduler:dmSchedulerList){
            ItemSchedulerVo itemSchedulerVo=new ItemSchedulerVo();
            BeanUtils.copyProperties(dmScheduler,itemSchedulerVo);
            itemSchedulerVo.setStartTime(dmScheduler.getStartTime().toString());
            itemSchedulerVo.setEndTime(dmScheduler.getEndTime().toString());
            itemSchedulerVoList.add(itemSchedulerVo);
        }
        return DtoUtil.returnDataSuccess(itemSchedulerVoList);
    }
}
