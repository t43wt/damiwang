package cn.dm.service.impl;

import cn.dm.client.*;
import cn.dm.common.*;
import cn.dm.dto.Dto;
import cn.dm.dto.DtoUtil;
import cn.dm.exception.ItemErrorCode;
import cn.dm.pojo.*;
import cn.dm.service.ItemDetailService;
import cn.dm.vo.ItemDetailVo;
import cn.dm.vo.ItemPriceVo;
import cn.dm.vo.ItemSchedulerVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ItemDetailServiceImpl implements ItemDetailService {

    @Autowired
    private RestDmItemClient dmItemClient;
    @Autowired
    private RestDmImageClient dmImageClient;
    @Autowired
    private RestDmCinemaClient restDmCinemaClient;
    @Autowired
    private RestDmSchedulerClient restDmSchedulerClient;
    @Autowired
    private RestDmSchedulerSeatPriceClient restDmSchedulerSeatPriceClient;
    @Autowired
    private RestDmSchedulerSeatClient restDmSchedulerSeatClient;

    @Override
    public Dto<ItemDetailVo> queryItemDetail(Long id) throws Exception {
        DmItem dmItem = dmItemClient.getDmItemById(id);
        if (EmptyUtils.isEmpty(dmItem)) {
            return null;
        }

        List<DmImage> dmImageList = getImageList(dmItem.getId(), Constants.Image.ImageType.carousel, Constants.Image.ImageCategory.item);

        DmCinema dmCinema = restDmCinemaClient.getDmCinemaById(dmItem.getCinemaId());

        ItemDetailVo itemDetailVo = copyData(dmItem, dmCinema, dmImageList);
        return DtoUtil.returnDataSuccess(itemDetailVo);
    }

    @Override
    public Dto<List<ItemSchedulerVo>> queryItemScheduler(Long id) throws Exception {

        DmItem dmItem = dmItemClient.getDmItemById(id);
        if (EmptyUtils.isEmpty(dmItem)) {
            throw new BaseException(ItemErrorCode.ITEM_NO_DATA);
        }

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("itemId", dmItem.getId());
        List<DmScheduler> dmSchedulerList = restDmSchedulerClient.getDmSchedulerListByMap(param);
        if (EmptyUtils.isEmpty(dmSchedulerList)) {
            throw new BaseException(ItemErrorCode.ITEM_NO_DATA);
        }

        List<ItemSchedulerVo> itemSchedulerVoList = new ArrayList<ItemSchedulerVo>();
        for (int i = 0; i < dmSchedulerList.size(); i++) {
            ItemSchedulerVo itemSchedulerVo = new ItemSchedulerVo();
            BeanUtils.copyProperties(dmItem, itemSchedulerVo);
            BeanUtils.copyProperties(dmSchedulerList.get(i), itemSchedulerVo);

            itemSchedulerVo.setStartTime(DateUtil.format(dmSchedulerList.get(i).getStartTime()));
            itemSchedulerVo.setEndTime(DateUtil.format(dmSchedulerList.get(i).getEndTime()));
            itemSchedulerVoList.add(itemSchedulerVo);
        }
        return DtoUtil.returnDataSuccess(itemSchedulerVoList);
    }

    @Override
    public Dto<List<ItemPriceVo>> queryItemPrice(Long id) throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("scheduleId", id);

        List<DmSchedulerSeatPrice> dmSchedulerSeatPriceList = restDmSchedulerSeatPriceClient.getDmSchedulerSeatPriceListByMap(param);
        if (EmptyUtils.isEmpty(dmSchedulerSeatPriceList)) {
            throw new BaseException(ItemErrorCode.ITEM_NO_DATA);
        }

        List<ItemPriceVo> itemPriceVoList = new ArrayList<ItemPriceVo>();
        for (DmSchedulerSeatPrice dmSchedulerSeatPrice : dmSchedulerSeatPriceList) {
            ItemPriceVo itemPriceVo = new ItemPriceVo();
            BeanUtils.copyProperties(dmSchedulerSeatPrice, itemPriceVo);

            Map<String, Object> lockMap = new HashMap<String, Object>();
            lockMap.put("status", 1);
            lockMap.put("scheduleId", itemPriceVo.getScheduleId());
            int num = restDmSchedulerSeatClient.getDmSchedulerSeatCountByMap(lockMap);
            int isHaveSeat = num > 0 ? 1 : 0;
            itemPriceVo.setIsHaveSeat(isHaveSeat);
            itemPriceVoList.add(itemPriceVo);
        }
        return DtoUtil.returnDataSuccess(itemPriceVoList);
    }

    private ItemDetailVo copyData(DmItem dmItem, DmCinema dmCinema, List<DmImage> dmImageList) throws ParseException {
        ItemDetailVo itemDetailVo = new ItemDetailVo();
        BeanUtils.copyProperties(dmItem, itemDetailVo);
        itemDetailVo.setStartTime(DateUtil.format(dmItem.getStartTime()));
        itemDetailVo.setEndTime(DateUtil.format(dmItem.getEndTime()));
        itemDetailVo.setState(dmItem.getState() + "");
        itemDetailVo.setImgUrl(dmImageList.get(0).getImgUrl());
        if (EmptyUtils.isEmpty(dmCinema)) {
            BeanUtils.copyProperties(dmCinema, itemDetailVo);
        }
        itemDetailVo.setId(dmItem.getId());
        return itemDetailVo;
    }

    public List<DmImage> getImageList(Long id, Integer type, Integer category) throws Exception {
        Map<String, Object> paramImage = new HashMap<String, Object>();
        paramImage.put("targetId", id);
        paramImage.put("type", type);
        paramImage.put("category", category);
        return dmImageClient.queryDmImageList(id, type, category);
    }
}
