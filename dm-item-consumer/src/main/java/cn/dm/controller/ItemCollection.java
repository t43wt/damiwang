package cn.dm.controller;

import cn.dm.dto.Dto;
import cn.dm.service.ItemLocalService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/item")
public class ItemCollection {
    @Resource
    private ItemLocalService itemLocalService;
    @RequestMapping(value = "/getItem",method = RequestMethod.POST)
    public Dto getItemById(String id) throws Exception{

        return itemLocalService.getItemInfo(Long.valueOf(id));
    }
    @RequestMapping(value = "/getScheduler",method = RequestMethod.POST)
    public Dto getScheduler(String itemid) throws Exception{

        return itemLocalService.getScheduler(Long.valueOf(itemid));
    }

}
