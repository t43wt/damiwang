package cn.dm.service;

import cn.dm.pojo.DmOrder;
import cn.dm.vo.DmItemMessageVo;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface DmTradeService {

    public Integer insertTrade(String orderNo, String tradeNo, Integer payMethod) throws Exception;

    public DmOrder loadDmOrderByOrderNo(String orderNo) throws Exception;

    public boolean processed(String orderNo, Integer flag) throws Exception;

}

