package cn.dm.service.impl;

import cn.dm.client.RestDmOrderClient;
import cn.dm.client.RestDmTradeClient;
import cn.dm.common.BaseException;
import cn.dm.common.Constants;
import cn.dm.exception.PayErrorCode;
import cn.dm.pojo.DmOrder;
import cn.dm.service.DmTradeService;
import cn.dm.vo.DmItemMessageVo;
import com.alipay.api.internal.util.AlipaySignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Service
public class DmTradeServiceImpl implements DmTradeService {
    private static final Logger logger = LoggerFactory.getLogger(DmTradeServiceImpl.class);
    @Resource
    private RestDmTradeClient restDmTradeClient;
    @Resource
    private RestDmOrderClient restDmOrderClient;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public Integer insertTrade(String orderNo, String tradeNo, Integer payMethod) throws Exception {

        DmOrder dmOrder = loadDmOrderByOrderNo(orderNo);

        if (2 == dmOrder.getOrderType()) {
            return 0;
        }
        DmItemMessageVo dmItemMessageVo = new DmItemMessageVo();
        dmItemMessageVo.setTradeNo(tradeNo);
        dmItemMessageVo.setOrderNo(orderNo);
        dmItemMessageVo.setItemId(dmOrder.getItemId().toString());
        dmItemMessageVo.setUserId(dmOrder.getUserId().toString());
        dmItemMessageVo.setAmount(dmOrder.getTotalAmount());
        dmItemMessageVo.setPayMethod(payMethod);

        this.updateOrderStatus(dmItemMessageVo);
        return restDmTradeClient.insertTrade(dmItemMessageVo);
    }


    public void updateOrderStatus(DmItemMessageVo dmItemMessageVo) throws Exception{

        DmOrder dmOrder = restDmOrderClient.getDmOrderByOrderNo(dmItemMessageVo.getOrderNo());


        while( 2!= dmOrder.getOrderType()){
            dmOrder = restDmOrderClient.getDmOrderByOrderNo(dmItemMessageVo.getOrderNo());
            System.out.println(">>>>>>>>>>>>>>>>>发消息修改订单状态");
            rabbitTemplate.convertAndSend(Constants.RabbitQueueName.TOPIC_EXCHANGE,
                    "key.toUpdateOrder",
                    dmItemMessageVo);
        }
    }

    @Override
    public DmOrder loadDmOrderByOrderNo(String orderNo) throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orderNo", orderNo);

        List<DmOrder> dmOrders = restDmOrderClient.getDmOrderListByMap(params);
        if (dmOrders.size() == 0) {
            throw new BaseException(PayErrorCode.PAY_NO_EXISTS);
        }
        return dmOrders.get(0);
    }

    @Override
    public boolean processed(String orderNo, Integer flag) throws Exception {
        return restDmOrderClient.processed(orderNo, flag);
    }
}
